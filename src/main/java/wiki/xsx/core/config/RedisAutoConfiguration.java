package wiki.xsx.core.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import wiki.xsx.core.config.cache.RedisCacheConfiguration;
import wiki.xsx.core.config.jedis.JedisConnectionConfiguration;
import wiki.xsx.core.config.lettuce.LettuceConnectionConfiguration;
import wiki.xsx.core.config.redisson.RedissonAutoConfiguration;
import wiki.xsx.core.util.ApplicationContextUtil;

/**
 * redis自动配置
 * @author xsx
 * @date 2019/4/18
 * @since 1.8
 */
@Configuration
@ConditionalOnClass({RedisTemplate.class})
@Import({
        ApplicationContextUtil.class,
        LettuceConnectionConfiguration.class,
        JedisConnectionConfiguration.class,
        RedissonAutoConfiguration.class,
        RedisCacheConfiguration.class
})
public class RedisAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean({RedisTemplate.class})
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        JsonRedisSerializer jsonRedisSerializer = new JsonRedisSerializer();
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setKeySerializer(RedisSerializer.string());
        template.setValueSerializer(jsonRedisSerializer);
        template.setHashKeySerializer(RedisSerializer.string());
        template.setHashValueSerializer(jsonRedisSerializer);
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

    @Bean
    @ConditionalOnMissingBean({StringRedisTemplate.class})
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        StringRedisTemplate template = new StringRedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }
}
